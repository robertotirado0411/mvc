/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.Fecha;

import vista.vdlgFecha;

import java.awt.event.ActionEvent;

// Importar clase e interface que se imp

// para escuchar a la vista.

import java.awt.event.ActionListener;

import javax.swing.JFrame;

import javax.swing.JOptionPane;
/**
 *
 * @author Edgar Guerrero
 */
public class Controlador implements ActionListener{
    private Fecha hoy;
    private vdlgFecha vista;
    
    public Controlador(Fecha hoy, vdlgFecha vista){
        this.hoy=hoy;
        this.vista=vista;
        //Hace que el controlador escuche los eventos de los botones
        // Que esten en la vista
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        
    }
    
private void iniciarVista(){ 
	vista.setTitle("Fecha "); 
	vista.setVisible(true);
}

@Override

public void actionPerformed(ActionEvent e) {

	if(e.getSource()== vista.btnGuardar) { 
	hoy.setDia (Integer.parseInt(vista.txtDia.getText())); 
	hoy.setMes (Integer.parseInt(vista.txtMes.getText())); 
	hoy.setAño (Integer.parseInt(vista.txtAño.getText()));

	}

	if(e.getSource()== vista.btnMostrar) {

	System.out.println("en mostrar");

	vista.txtDia.setText(String.valueOf(hoy.getDia()));

	vista.txtMes.setText(String.valueOf(hoy.getMes()));

	vista.txtAño.setText(String.valueOf(hoy.getAño()));

	if(hoy.esBisiesto()== true) JOptionPane.showMessageDialog(vista, "Es un año Bleiesto");
	}

	if(e.getSource()== vista.btnLimpiar) {

	vista.txtDia.setText("");

	vista.txtMes.setText("");

	vista.txtAño.setText("");
	}
}

public static void main (String[] args){
    Fecha hoy = new Fecha();
    vdlgFecha vista= new vdlgFecha(new JFrame(), true);
    
    Controlador control = new Controlador (hoy,vista);
    control.iniciarVista();
}



}
